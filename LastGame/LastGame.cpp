// Copyright Epic Games, Inc. All Rights Reserved.

#include "LastGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LastGame, "LastGame" );
