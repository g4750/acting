// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LastGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LASTGAME_API ALastGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
